CREATE TABLE `zhihuanswer` (
  `zhihu_id` int(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  `question_id` varchar(25) NOT NULL,
  `author_id` varchar(50) DEFAULT NULL,
  `content` longtext NOT NULL,
  `parise_num` varchar(20) DEFAULT NULL,
  `comment_count` varchar(20) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `crawl_time` datetime NOT NULL,
  PRIMARY KEY (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
