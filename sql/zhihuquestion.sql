CREATE TABLE `zhihuquestion` (
  `zhihu_id` int(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `upvoteCount` varchar(20) DEFAULT NULL,
  `answer_num` varchar(15) DEFAULT NULL,
  `comments_num` varchar(15) DEFAULT NULL,
  `follower_num` varchar(15) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  `crawl_time` datetime NOT NULL,
  PRIMARY KEY (`zhihu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;