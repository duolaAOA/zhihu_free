# -*- coding: utf-8 -*-
import scrapy
from w3lib.html import remove_tags
import logging
import time
from PIL import Image
import json
import re
from datetime import datetime

from scrapy.utils.project import get_project_settings
from zhihu.items import ZhihuQuestionItem,QuestionItemLoader,ZhihuAnswerItem

class ZhihuspiderSpider(scrapy.Spider):
    name = "zhihuspider"
    allowed_domains = ["www.zhihu.com"]
    start_urls = ['http://www.zhihu.com/']

    settings = get_project_settings()
    # 下一页api
    next_page = settings['NEXT_PAGE']
    #下一页携带token
    session_token = ''
    #回答api
    answer_api = settings['ANSWER_API']

    headers = settings['HEADERS']
    #爬取最大深度
    max_deepth = settings['MAX_DEEPTH']

    def parse(self, response):

        logger = logging.getLogger(__name__)
        logger.info('Parse function called on %s', response.url)
        all_urls = response.css("div.ContentItem.AnswerItem > h2 > div >a::attr(href)").extract()

        #所有问答链接
        for url in all_urls:
            question_url = 'https://www.zhihu.com' + url
            yield scrapy.Request(question_url,headers=self.headers,callback=self.parse_question)

        token, authorization_id = self.parse_next_page(response)
        self.headers['authorization'] = authorization_id
        self.session_token = token
        #限制每次获取个数
        after_id = 10
        while after_id < self.max_deepth:
            yield scrapy.Request(self.next_page.format(self.session_token, after_id), headers=self.headers, callback=self.paser_next_question)

            after_id +=10
    def parse_next_page(self,response):

        '''解析下一页paramdata'''
        a = re.findall('session_token=([A-Za-z0-9]+)', response.text)[0]
        b ='Bearer ' + re.findall(r'carCompose&quot;:&quot;(.*?)&quot', response.text)[0]
        return a, b

    def parse_question(self,response):
        match_obj = re.search('\d+',response.url)
        if match_obj:
            question_id = int(match_obj.group())
        item_loader = QuestionItemLoader(item=ZhihuQuestionItem(), response=response)
        item_loader.add_css("title", "#root > div > main > div > meta:nth-child(1)::attr(content)")
        item_loader.add_value("url", response.url)
        item_loader.add_value("zhihu_id", question_id)
        item_loader.add_css("keywords", "#root meta:nth-child(3)::attr(content)")
        item_loader.add_xpath("upvoteCount", '//div[@class="ContentItem AnswerItem"]/meta[2]/@content')
        item_loader.add_css("answer_num","#root meta:nth-child(4)::attr(content)")
        item_loader.add_css("comments_num","#root meta:nth-child(5)::attr(content)")
        item_loader.add_css("follower_num", "#root meta:nth-child(9)::attr(content)")
        item_loader.add_css("create_time","#root meta:nth-child(6)::attr(content)")
        item_loader.add_value("crawl_time", datetime.strftime(datetime.now(),'%Y-%m-%d %H:%M:%S'))

        question_item = item_loader.load_item()

        yield question_item
        yield scrapy.Request(url=self.answer_api.format(question_id,3,20), headers=self.headers, callback=self.parse_answer)


    def parse_answer(self,response):
        '''
        处理question的answer
        '''
        answer_json = json.loads(response.text)
        is_end = answer_json["paging"]["is_end"]
        next_url = answer_json["paging"]["next"]

        for answer in answer_json["data"]:
            answer_item = ZhihuAnswerItem()
            answer_item["zhihu_id"] = answer["id"]
            answer_item["url"] = answer["url"]
            answer_item["question_id"] = answer["question"]["id"]
            answer_item["author_id"] = answer["author"]["id"] if "id" in answer["author"] else None
            answer_item["content"] = remove_tags(answer["content"] if "content" in answer else None)
            answer_item["parise_num"] = answer["voteup_count"]
            answer_item["comment_count"] = answer["comment_count"]
            g_time = answer["created_time"]
            answer_item["create_time"] =self.get_answer_time(g_time)
            answer_item["crawl_time"] = datetime.strftime(datetime.now(),'%Y-%m-%d %H:%M:%S')

            yield answer_item
            if not is_end:
                yield scrapy.Request(url=next_url, headers=self.headers, callback=self.parse_answer)


    def get_answer_time(self,g_time):
        b = re.search('\d{4}-\d{2}-\d{2}', str(datetime.fromtimestamp(g_time))).group()
        return b

    def paser_next_question(self,response):

        questions = json.loads(response.text)
        for i in questions['data']:
            try:
                question_api = i["target"]["question"]["url"]
                question_url = re.sub('api', 'www',question_api)
            except Exception as e:
                print(e)
            yield scrapy.Request(url=question_url, headers=self.headers,
                                 callback=self.parse_question)

    def start_requests(self):
        return [scrapy.Request('https://www.zhihu.com/#signin',headers=self.headers,callback=self.login_front)]


    def login_front(self,response):
        '''登录'''
        response_text = response.text
        match_obj = re.findall(r'name="_xsrf" value="(.*?)"', response_text)[0]
        xsrf = ''
        if match_obj:
            xsrf =  match_obj
        if xsrf:
            self.headers["X-Xsrftoken"] = xsrf
            post_data = {
                '_xsrf': xsrf,
                'password': 'admin123',
                'phone_num': '13079789780',
                'captcha_type': 'cn',
            }

            t = str(int(time.time() * 1000))
            captcha_url = "https://www.zhihu.com/captcha.gif?r={}&type=login&lang=cn".format(t)
            yield scrapy.Request(captcha_url,headers=self.headers,meta={'post_data':post_data},
                                 callback=self.login_after_captcha)

    def login_after_captcha(self,response):
        '''登录'''
        login_data = response.meta.get('post_data',{})
        login_data['captcha'] = self.get_captcha(response)
        login_url = 'https://www.zhihu.com/login/phone_num'

        return [scrapy.FormRequest(
            url=login_url,
            formdata=login_data,
            headers=self.headers,
            callback=self.check_login_after_captcha
        )]

    def get_captcha(self,response):
        '''获取验证码'''
        with open('captcha_scrapy.jpg','wb') as f:
            f.write(response._body)
        try:
            im = Image.open('captcha_scrapy.jpg')
            im.show()
        except Exception as e:
            print(e)
            pass

        captcha_input = {
            'img_size': [200, 44],
            'input_points': [],
        }
        captcha_param= [
            [12.95, 14.969999999999998],
            [36.1, 16.009999999999998],
            [57.16, 24.44],
            [84.52, 19.17],
            [108.72, 28.64],
            [132.95, 24.44],
            [151.89, 23.380000000000002]
        ]
        seq = input('请输入倒立字的位置\n>')
        for i in seq.split():
            captcha_input['input_points'].append(captcha_param[int(i) - 1])
        return json.dumps(captcha_input)

    def check_login_after_captcha(self,response):
        # 验证服务器返回数据判断是否成功
        text_json = json.loads(response.text)
        if text_json['r'] == 0:
            for url in self.start_urls:
                yield scrapy.Request(url,dont_filter=True,headers=self.headers)#此url不去重
        else:
            print("验证码错误，登录失败！")
