# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from twisted.enterprise import adbapi
import pymysql.cursors
import pymysql


class ZhihuPipeline(object):
    def process_item(self, item, spider):
        return item


class MyTwistedPipeline(object):
    def __init__(self,dbpool):
        self.dbpool = dbpool


    @classmethod
    def from_settings(cls,settings):
        dbparms = dict(
            host = settings["MYSQL_HOST"],
            db = settings["MYSQL_DBNAME"],
            user = settings["MYSQL_USER"],
            passwd = settings["MYSQL_PASSWORD"],
            port = settings["MYSQL_PORT"],
            charset='utf8',
            cursorclass=pymysql.cursors.DictCursor,
            use_unicode=True,
        )

        dbpool = adbapi.ConnectionPool("pymysql", **dbparms)
        return cls(dbpool)


    def process_item(self, item, settings):

        query = self.dbpool.runInteraction(self.insert_data, item)
        query.addErrback(self.handle_error)


    def handle_error(self,failure):
        print(failure)



    def insert_data(self, cursor, item ):

        if item.__class__.__name__ == 'ZhihuQuestionItem':

            insert_sql = """
                    insert into zhihu.zhihuquestion(zhihu_id, url, title, keywords, upvoteCount,
                                 answer_num, comments_num, follower_num, create_time, crawl_time
                    ) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE answer_num=VALUES (answer_num),
                        upvoteCount = VALUES (upvoteCount), comments_num = VALUES (comments_num), follower_num = VALUES (follower_num)
                """

            cursor.execute(insert_sql, (item['zhihu_id'], item['url'], item['title'], item['keywords'],
                                      item['upvoteCount'], item['answer_num'], item['comments_num'],
                                      item['follower_num'], item['create_time'], item['crawl_time']))
            print("Insert a question data successfully！")

        else:

            insert_sql = """
                        insert into zhihu.zhihuanswer(zhihu_id, url, question_id, author_id, content, parise_num,
                          comment_count, create_time, crawl_time
                          )values (%s, %s, %s, %s, %s, %s, %s, %s, %s)ON DUPLICATE KEY UPDATE parise_num=VALUES (parise_num),
                            comment_count = VALUES (comment_count)
                        """

            cursor.execute(insert_sql, (item['zhihu_id'], item['url'], item['question_id'], item['author_id'],
                                  item['content'], item['parise_num'], item['comment_count'], item['create_time'],
                                  item['crawl_time']))
            print("Insert a answer data successfully！")

