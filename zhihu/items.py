# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst, MapCompose, Join
from scrapy.loader import ItemLoader
import re


def get_time(value):
    ctime = re.search('\d{4}-\d{2}-\d{2}', value).group()
    return ctime

def get_question_url(value):
    return value.split("/a")[0]

class QuestionItemLoader(ItemLoader):
    default_output_processor = TakeFirst()


class ZhihuQuestionItem(scrapy.Item):
    zhihu_id = scrapy.Field()
    url = scrapy.Field(
        input_processor=MapCompose(get_question_url)
    )
    title   = scrapy.Field()
    keywords = scrapy.Field()
    upvoteCount = scrapy.Field()
    answer_num  = scrapy.Field()
    comments_num = scrapy.Field()
    follower_num  = scrapy.Field()
    create_time = scrapy.Field(
        input_processor = MapCompose(get_time)
    )
    crawl_time  = scrapy.Field()


class ZhihuAnswerItem(scrapy.Item):
    zhihu_id  = scrapy.Field()
    url =  scrapy.Field()
    question_id  = scrapy.Field()
    author_id  = scrapy.Field()
    content  = scrapy.Field()
    parise_num = scrapy.Field()
    comment_count  = scrapy.Field()
    create_time  = scrapy.Field()
    crawl_time  = scrapy.Field()




